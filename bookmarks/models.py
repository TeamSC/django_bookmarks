from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible
class Link(models.Model):
    url = models.URLField(unique=True)

    def __str__(self):
        return self.url

@python_2_unicode_compatible
class Bookmark(models.Model):
    title = models.CharField(max_length=200)
    user = models.ForeignKey(User)
    link = models.ForeignKey(Link)

    def __str__(self):
        return '%s, %s' % (self.user.username, self.link.url)

@python_2_unicode_compatible
class Tag(models.Model):
    name = models.CharField(max_length=64, unique=True)
    bookmarks = models.ManyToManyField(Bookmark)

    def __str__(self):
        return self.name
