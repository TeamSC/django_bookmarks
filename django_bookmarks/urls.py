# The file imports everything from the module django.conf.urls.defaults.
# This module provides the necessary functions to define URLs.
import os.path
from django.conf.urls import *
from django.contrib.auth import views as auth_views
from django.views.static import serve
from django.views.generic import TemplateView

# We import everything from bookmarks.views.
# This is necessary to access our views, and connect them to URLs.
from bookmarks import *
from bookmarks.views import *


site_media = os.path.join(
    os.path.dirname(__file__), 'site_media'
)

urlpatterns = [
    #Browsing
    url(r'^$', main_page),
    url(r'^tag/([^\s]+)/$', tag_page),
    url(r'^user/(\w+)/$', user_page),
    url(r'^tag/$', tag_cloud_page),
    url(r'^search/$', search_page),

    #Session management
    url(r'^login/$', auth_views.login, name='login'),
    url(r'^logout/$', logout_page),
    url(r'^site_media/(?P<path>.*)$', serve, { 'document_root': site_media }),
    url(r'^register/$', register_page),
    url(r'^register/success/$', TemplateView.as_view(template_name='registration/register_success.html')),

    #Account management
    url(r'^save/$', bookmark_save_page),
]
